<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Repositories\CurlConnectionRepository;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Create a new controller instance.
     *
     * @param CurlConnectionRepository $curl_connect
     */
    public function __construct(CurlConnectionRepository $curl_connect)
    {
        $this->middleware('guest');
        $this->curl_connect = $curl_connect;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    public function register(){
        if (!isset($_COOKIE['auth_user'])){
            return view('backend.register');
        }
        return redirect('/login');
    }
    public function postRegister(Request $request){
        try{
        $option = 1;
        $url_login  ='http://127.0.0.1:8000/api/register';
        $param = [
            'name' => strip_tags($request->username),
            'email' => strip_tags($request->email),
            'password' => strip_tags($request->password),
            'c_password' => strip_tags($request->password_confirm),
        ];
        $curl = $this->curl_connect->curl_set_post($option, $url_login, $param, "POST");
        $res = $this->curl_connect->curl_get($curl);
        if (isset($res->success)) {
            setcookie("auth_user", $res->success->token, time()+5 * 365 * 24 * 60 * 60);
            return redirect('/login');
        }
        else{
            return redirect('/login')->back()->with('error', ' Đăng ký thất bại');
        }

    } catch (\Exception $e) {
        return redirect('/login')->back()->with('error', ' Đăng nhập thất bại');
        }
    }
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }
}
