<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Repositories\CurlConnectionRepository;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Create a new controller instance.
     *
     * @param CurlConnectionRepository $curl_connect
     */
    public function __construct(CurlConnectionRepository $curl_connect)
    {
        $this->middleware('guest')->except('logout');
        $this->curl_connect = $curl_connect;
    }
    public function login() {
        if (!isset($_COOKIE['auth_user'])){
            return view('backend.login');
        }
        else{
            if (isset($_COOKIE['auth_user'])) {
//                dd($_COOKIE['auth_user']);
                try {
                    if (isset($_COOKIE['auth_user'])) {
                        $token = 'Authorization:Bearer '. $_COOKIE['auth_user'];
//                        dd($token);
                        $url_get_token ='http://127.0.0.1:8000/api/details';
                        $curl = $this->curl_connect->curl_set_get(1, $url_get_token, $token);
                        $res = $this->curl_connect->curl_get($curl);
                        if (isset($res)){
                            return view('backend.home',['user_login' => $res]);
                        }
                    }
                }catch (\Exception $e){
                    dd('error');
                }
            }
                else {
                    return redirect('backend.login');
                }
            }
    }
    public function postLogin(Request $request) {
        try {
//            if ($this->replace_string( $request->email)==true||$this->replace_string( $request->password)==true) {
//                if (!isset($_COOKIE['error_capcha'])&& !isset($request->code)) {
//                    setcookie("error_capcha", 1, time() + 3600);
//                } else {
//                    setcookie("error_capcha", intval($_COOKIE["error_capcha"]) + 1, time() + 3600);
//                }
//                return view('login',
//                    [
//                        'err_s' =>'Vui lòng nhập đầy đủ và đúng định dạng',
//                    ]);
//            }
            $option = 1;
            $url_login  ='http://127.0.0.1:8000/api/login';
            $param = [
                'email' => strip_tags($request->email),
                'password' => strip_tags($request->password),

            ];
//
            $curl = $this->curl_connect->curl_set_post($option, $url_login, $param, "POST");
            $res = $this->curl_connect->curl_get($curl);
            if (isset($res->success->code) && $res->success->code == 200) {
                setcookie("auth_user", $res->success->token, time()+5 * 365 * 24 * 60 * 60);
                    return redirect('/login');
                }
                else{
                    return redirect('/login');
                }

            } catch (\Exception $e) {
            return redirect()->back()->with('error', ' Đăng nhập thất bại');
        }
    }
    public function logout(Request $request)
    {
        setcookie("auth_user", 1, time() -36000);
        return redirect('/login');
    }

}
