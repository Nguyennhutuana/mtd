<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use App\Models\product_categories;
use App\Models\slides;
use Illuminate\Http\Request;
use App\Models\products;
class homeController extends Controller
{
    public function index(){
        $data['products'] = products::all();
        $data['categories'] = product_categories::all();
        $data['slides'] = slides::all();
    	return view('frontend.index.home',$data);
    }
}
