<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use App\Models\product_categories;
use App\Models\products;
use App\Models\slides;
use Illuminate\Http\Request;

class shopController extends Controller
{
    public function index(){
        $data['products'] = products::paginate(9);
        $data['categories'] = product_categories::all();
    	return view('frontend.shop.shop',$data);
    }
}
