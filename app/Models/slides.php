<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class slides extends Model
{
    protected $table = 'slides';
    protected $primarykey = 'id';
    protected $guarded = [];
}
