<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->Increments('id');
            $table->string('name');
            $table->string('image');
            $table->string('short_describe');
            $table->string('description');
            $table->string('price');
            $table->string('feature');
            $table->string('link_product_use');
            $table->string('link_product_detail');
            $table->string('link_document');
            $table->tinyInteger('status');
            $table->integer('landing_pages');
            $table->integer('star_vote');
            $table->string('note');
            $table->integer('product_category');
            $table->integer('product_view');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
