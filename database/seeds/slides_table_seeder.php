<?php

use Illuminate\Database\Seeder;

class slides_table_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        for ($i = 0; $i < 3; $i++) {
            \DB::table('slides')->insert([
                'name' => $faker->text(10),
                'title' => $faker->text(10),
                'link' => $faker->text(10),
                'image' => ('slide'.$faker->numberBetween(1,3).'.jpg'),
                'product_id' => $faker->numberBetween(1, 10),
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                ]);
        }
    }
}
