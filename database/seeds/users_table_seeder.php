<?php

use Illuminate\Database\Seeder;

class users_table_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name'=>'nhuanh',
                'email'=>'nhuanh@gmail.com',
                'password'=>bcrypt('123456'),
            ],
            [
                'name'=>'khanhlinh',
                'email'=>'khanhlinh@gmail.com',
                'password'=>bcrypt('123456'),
            ],
        ];
        DB::table('users') -> insert($data);
    }
}
