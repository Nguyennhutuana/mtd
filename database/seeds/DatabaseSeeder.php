<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(users_table_seeder::class);
        $this->call(product_categories_table_seeder::class);
        $this->call(products_Table_seeder::class);
        $this->call(slides_table_seeder::class);
    }
}
