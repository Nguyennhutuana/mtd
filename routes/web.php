<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group([], function () {
    Route::group([], function () {
        Route::get('/', 'frontend\homeController@index')->name('home');
    });
    Route::group(['prefix' => 'shop'], function () {
        Route::get('/', 'frontend\shopController@index')->name('shop');
    });
        Route::group(['prefix' => 'blog'], function () {
        Route::get('/', 'frontend\blogController@index')->name('blog');
    });
            Route::group(['prefix' => 'contact'], function () {
        Route::get('/', 'frontend\contactController@index')->name('contact');
    });
                Route::group(['prefix' => 'about'], function () {
        Route::get('/', function () {
        return view('frontend.about.about');
    })->name('about');
    });
});
Route::group(['namespace'=>'Auth'], function(){
    Route::get('/login', 'LoginController@login')->name('login');
    Route::get('/register', 'RegisterController@register')->name('register');
    Route::post('/login', 'LoginController@postLogin')->name('post-login');
    Route::post('/register', 'RegisterController@postRegister')->name('post-register');
    Route::get('/logout', 'LoginController@logout')->name('logout');
    Route::group(['prefix'=>'admin', 'middleware'=>'CheckLogedIn'],function(){
    });
});
